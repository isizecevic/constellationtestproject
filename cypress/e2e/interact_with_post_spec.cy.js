describe('Interact with a post', () => {

  beforeEach('log in', ()=> {
    cy.visit('login')
    cy.get('#email').type(Cypress.env('email'))
    cy.get('#password').type(Cypress.env('password'))
    cy.get('#loginSubmitBtn').click()  
  })

  it('like and unlike', () => {  
    cy.get('.post__actions').first().should('have.text', '00')
    cy.get('[data-icon="heart"]').first().click()
    cy.get('.post__actions').first().should('have.text', '10')
    cy.get('[data-icon="heart"]').first().click()
    cy.get('.post__actions').first().should('have.text', '00')
  })

  it('create and delete comment', () => {
    let comment = 'First!'
    cy.get('[data-icon="comment"]').first().click()
    cy.get('.post__actions').first().should('have.text', '00')
    cy.get('[placeholder="Write a comment"]').type(comment)
    cy.get('[data-icon="paper-plane"]').click()
    cy.get('.post__actions').first().should('have.text', '01')
    cy.get('.post__comments__list__comment__body').should('have.text', comment)
    cy.get('#postDeleteBtn').click()
    cy.get('.post__actions').first().should('have.text', '00')
  })
})