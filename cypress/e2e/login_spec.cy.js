describe('Successful login', () => {
  it('enter valid credentials', () => {
    cy.visit('login')
    cy.get('#email').type(Cypress.env('email'))
    cy.get('#password').type(Cypress.env('password'))
    cy.get('#loginSubmitBtn').click()
    cy.get('.home__main__feed').should('be.visible')
  })
})