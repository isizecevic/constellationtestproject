describe('Create a new post', () => {

  beforeEach('log in', ()=> {
    cy.visit('login')
    cy.get('#email').type(Cypress.env('email'))
    cy.get('#password').type(Cypress.env('password'))
    cy.get('#loginSubmitBtn').click()  
  })

  it('create a text post', () => {
   let postContent = 'Hello world (text only)!'
    cy.get('[placeholder="What\'s happening"]').type(postContent)
    cy.get('#submitPostBtn').click()
    cy.get('.post__description').first().should("have.text", postContent)
  })

  it('create a post with audio', () => {
    let postContent = 'Hello world (audio)!'
    cy.get('[placeholder="What\'s happening"]').type(postContent)
    cy.get('#startRecordingButton').click()
    cy.wait(500)
    cy.get('#stopRecordingButton').click()
    cy.get('#submitPostBtn').click()
    cy.get('.post__description').first().should("have.text", postContent)
  })
})