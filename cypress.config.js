const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    baseUrl: 'http://constel-social-network.vercel.app',
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
